declare option output:omit-xml-declaration "no";
declare variable $baza-url external := "mankas";
<ekzamenoj>{
  for $ekzameno in /ekzamenoj/ekzameno
  let $dato := data($ekzameno/dato)
  return
  <ekzameno>
    {$ekzameno/dato}
    <url>{concat($baza-url, "/ekzamenoj/", $dato, ".pdf")}</url>
    <bildeto>{concat($baza-url, "/problemoj-bildeto/", $dato, "-bildeto.png")}</bildeto>
    <problemoj>{
      for $problemo in $ekzameno/problemoj/problemo
      let $numero := substring(string(100 + data($problemo/numero)), 2)
      return
      <problemo>
        {$problemo/klaso}
        {$problemo/numero}
        {$problemo/branĉo}
        {$problemo/temo}
        <url>{concat($baza-url, "/problemoj/", $dato, "-", $numero, ".png")}</url>
        <bildeto>{concat($baza-url, "/problemoj-bildeto/", $dato, "-", $numero, "-bildeto.png")}</bildeto>
      </problemo>
    }</problemoj>
  </ekzameno>
}</ekzamenoj>
