# Архив с изпити и задачи давани на НВО по математика след 7 клас.

Това хранилище съдържа:
1. Всички давани Национални външни оценявания по математика след 7 клас от 2010 година до днес. Изтеглени са от сайта на <a href="https://mon.bg/bg/100149"><img src="http://back2school.mon.bg/assets/images/MON-LogoSmall.png" alt="Министерство на образованието и науката" height="50px"/></a>.
2. Всички вече давани задачи от тези изпити - изрязани и записани, всяка като отделно [PNG](https://bg.wikipedia.org/wiki/PNG) изображение.
3. Скриптовете, с които полуавтоматизирано изрязах отделните задачи, както и подробни описания за етапите, през които преминах. Описах ги с надеждата да ви послужат, ако изберете да се захванете с подобно начинание, както и за да си припомня как съм изпълнил задачата, ако ми се наложи и занапред.

За обработката на изображенията ползвах само <a href="https://www.gnu.org/philosophy/free-sw.bg.html">свободен (libre) софтуер <img src="https://www.gnu.org/graphics/reiss-head-sm.jpg" alt="ГНУ" height="70px"/></a>:
1. <a href="https://arcolinux.com/"><img src="https://upload.wikimedia.org/wikipedia/commons/7/7d/Arcolinux.svg" alt="Arco GNU+Linux" height="30px"/>rco GNU+Linux</a> - операционната система
2. <a href="https://www.gnu.org/software/bash/"><img src="https://upload.wikimedia.org/wikipedia/commons/8/82/Gnu-bash-logo.svg" alt="Bash" height="30px"/></a> - скриптовият език
3. <a href="https://imagemagick.org/index.php"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/ImageMagick_logo.svg/1200px-ImageMagick_logo.svg.png" alt="ImageMagic" height="30px"/>ImageMagic</a> - програмата за автоматизирана обработка на изображенията ... в BASH скриптовете
4. <a href="https://www.gimp.org/"><img src="https://upload.wikimedia.org/wikipedia/commons/4/45/The_GIMP_icon_-_gnome.svg" alt="GIMP" height="30px"/>GIMP</a> - за стъпките, които направих ръчно, защото автоматизирането им ми се струва прекалено сложно

Лицензът на всички скриптове, които ще откриете в това хранилище е също свободен:
[![GPLv3 or later](https://www.gnu.org/graphics/gplv3-or-later.svg)](https://www.gnu.org/licenses/gpl-3.0.en.html).

Надявам се, че много от вас ще изберат да ги ползват в свой проект или ще ми пишат с предложения за подобряването им.

Като основа за логото на това хранилище ползвах логото <a href="https://commons.wikimedia.org/wiki/File:Internet_Archive_logo_and_wordmark.svg"><img src="https://upload.wikimedia.org/wikipedia/commons/8/84/Internet_Archive_logo_and_wordmark.svg" height="50px"/></a>, като само премахнах надписите. Първоначалното изображение е <a href="https://bg.wikipedia.org/wiki/%D0%9E%D0%B1%D1%89%D0%B5%D1%81%D1%82%D0%B2%D0%B5%D0%BD%D0%BE_%D0%B4%D0%BE%D1%81%D1%82%D0%BE%D1%8F%D0%BD%D0%B8%D0%B5">обществено достояние</a>, което ми позволява да го ползвам като основа.

Това хранилище е част от [по-големия проект](https://gitlab.com/nvo-7-matematika) за изграждане на свободен сборник и уеб сайт със задачи по математика.
